import SwiftUI
import CoreData

struct ContentView: View {
    @Environment(\.managedObjectContext) var moc
    @FetchRequest(entity: Student.entity(), sortDescriptors: []) var students: FetchedResults<Student>
    
    var body: some View {
        VStack {
            List {
                ForEach(students, id: \.id) { student in
                    Text(student.name ?? "Unknown")
                }
            }
            Button("Add") {
                let firstNames = ["Luna","Ginny", "Harry", "Hermione", "Ron"]
                let lastNames = ["Lovegood", "Weasley", "Potter", "Granger"]
                let chosenName = firstNames.randomElement() ?? ""
                let chosenLastName = lastNames.randomElement() ?? ""
                
                let student = Student(context: self.moc)
                student.id = UUID()
                student.name = "\(chosenName) \(chosenLastName)"
                
                try? self.moc.save()
            }.padding()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
